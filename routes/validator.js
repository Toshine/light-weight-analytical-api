const { body, check, validationResult } = require("express-validator")

exports.dataValidation = () => {
    return [
        // body('.*.').notEmpty().withMessage('all fields are required')
        check('ip').notEmpty().withMessage('ip addresss is required'),
        check('coordinates.*').toInt().notEmpty().withMessage('coordinated must not be empty')
        // check('coordinates.*.y').notEmpty().withMessage('coordinate Y is required').toInt().withMessage('coordinate must be an integer')
        //check('createdAt').notEmpty().withMessage('date is required')
    ]
}

exports.validate = (req, res, next) => {
    const errors = validationResult(req)
    if (errors.isEmpty()) {
      return next()
    }
    const extractedErrors = []
    errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }))
  
    return res.status(422).json({
      errors: extractedErrors,
    })
  }

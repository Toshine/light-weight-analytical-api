const express = require('express')

const router = express.Router()
const Coordinates = require('../controller/CoordinatesController')
const { dataValidation, validate } = require('./validator.js')

router.post("/analytics", dataValidation(), validate, Coordinates.storeCoordinates)

router.get("/analytics/list", Coordinates.getCoordinates)

router.get("/analytics", Coordinates.getIpAddress)




module.exports = router
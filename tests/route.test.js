const app = require('../app.js')
const supertest = require('supertest')
const request = supertest(app)

it('Testing GET analytics route', async () => {
    const response = await request.get('/all/analytics')
    expect (response.status).toBe(200)
  })

  it('Testing POST analytics route', async () => {
    const response = await request.post('/analytics')
    .send({
        ip: "187.00.88.49",
        coordinates: {
            x: 89,
            y:58
        },
    })
    expect (response.status).toBe(200)
  })

  it('Testing GET ip address route', async () => {
    const response = await request.get('/analytics')
    expect (response.status).toBe(200)
  })
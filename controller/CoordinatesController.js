
const fs = require('fs')
const path = require('path')
const { exit } = require('process')
const { stringify } = require('qs')
const moment = require('moment')


// store ip address and coordinates to file
module.exports.storeCoordinates = (req, res) => {

    const { ip, coordinates } = req.body
    

    const data = {ip, coordinates, createdAt: new Date()}

    const absolutePath = path.resolve(__dirname, '../db/data.json')

    
    try{
        
        fs.writeFileSync(
        absolutePath, JSON.stringify(data) + '\r', 
        { flag: 'a'}
    )
    return res.status(200).json({
        message: 'Coordinate stored successfully'
    })

    }catch(err){
        return res.status(401).json(err)
    }

    
}


const getAllData = () => {
    const absolutePath = path.resolve(__dirname, '../db/data.json')
    const coordinates = []
    data = fs.readFileSync(absolutePath, 'utf8')
    fs.readFileSync(absolutePath, 'utf8').toString().split('\r').forEach(function(data){
        if (data.length > 1) {
            coordinates.push(JSON.parse(data))
        }
    });
    // return allEvents
    return coordinates
}

// retreive ip address and coordinates
module.exports.getCoordinates = (req, res) => {
    const coordinates = getAllData()
    return res.json({
        status: 'success',
        data: coordinates
    })
}

// Get distance for specified ip address
module.exports.getIpAddress = (req, res) => {
    let query = req.query.ip

    const arrCoordinates = getAllData()
    const ipAddress = arrCoordinates.filter(el => el.ip === query)

    

    let dateTime = ipAddress.map(function(time) {
        
        let myDateTime = new Date(time.createdAt)
        
        let myMoment = moment().subtract(1, 'hours')
        
        // Check if record is less than hour old
        if (myDateTime > myMoment && myDateTime < moment()){


            let firstCoordinates = ipAddress[0].coordinates
    
            let distance = 0
        
            ipAddress.forEach(el => {
    
            distance += getDistance(firstCoordinates.x, el.coordinates.x, firstCoordinates.y, el.coordinates.y)
            firstCoordinates = el.coordinates
          })
    
            return res.json({
                distance: distance
            })
        }
            return res.json({
                message: 'Invalid data'
            })
    })


    return res.json("No data found")
    
    
}

// Calculate distance
function getDistance(x1, x2, y1, y2) {
    return Math.hypot(x2-x1, y2-y1)
}



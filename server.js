const app = require('./app.js')

const port = process.env.PORT || 3030

app.listen(port, () => {
    console.log(`Your app is running on: ${port}`)
})
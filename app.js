const express = require('express');

const routes = require("./routes/index")

const app = express();

app.use(express.urlencoded({ extended:false }));
app.use(express.json());

app.use('/', routes)


const port = process.env.PORT || 3030


module.exports = app;